# Tricentis - Vehicle Insurance Application
Testes automatizados para o aplicativo de amostra disponivel em: http://sampleapp.tricentis.com/101/app.php

Esses testes foram realizados usando Selenium WebDriver (Java) e Cucumber.

Os testes contemplam  as seguintes etapas:

1- Acesso ao endereço: http://sampleapp.tricentis.com/101/app.php;
2- Preenchimento  do formulário na aba “Inserir Dados do Veículo” e clique em Avançar;
3- Preenchimento  do formulário na aba “Informe Dados do Segurado” e clique em Avançar;
4- Preenchimento  do formulário da aba “Entrar Dados do Produto” e clique em Avançar;
5- Preenchimento  do formulário na aba “Selecionar Opção de Preço” e clique em Avançar;
6- Preenchimento  do formulário na aba “Enviar Cotação” e clique em  Enviar;
7- Verificação da mensagem de sucesso exibida na tela

Anotações importantes:

* O driver padrão definido é o htmlunitdriver, que não possui uma interface gráfica.
* Os drivers padrão configurados para GUI são executáveis ​​apenas no sistema operacional mac;
* Foram incluídos drivers para o sistema operacional Windows, mas devem ser configurados em BrowserFactory.class

=======================================================================================================================================

Automated tests for the sample application presented in: http://sampleapp.tricentis.com/101/app.php

These tests was realized using Selenium WebDriver (Java) and Cucumber.

The tests include the following steps:

1- Access : http://sampleapp.tricentis.com/101/app.php;
2- Fill in the form on tab "Enter Vehicle Data" and press Next;
3- Fill in the form on tab "Enter Insurant Data" and press Next;
4- Fill in the form on tab "Enter Product Data" and press Next;
5- Fill in the form on tab "Select Price Option" and press Next;
6- Fill in the form on tab "Send Quote" and press Send;
7- Check success message displayed on screen

Important Notes:

*The default Driver set is the htmlunitdriver, which does not have a graphical interface.
*The default Drivers configured for GUI are only executable on mac operating system;
*Executable drivers were included for the windows operating system, but must be set on BrowserFactory.class
