package com.tricentis.vehicleinsuranceapp.browser;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

public class Browser {

private static WebDriver driver;
	
	public Browser() {
		Browser.driver = new BrowserFactory().criarWebDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
	
	public void iniciarBrowser(String url) {
		driver.get(url);
	}
	
	public static void encerrarBrowser() {
		getDriver().manage().deleteAllCookies();
		getDriver().close();
	}
	
}
