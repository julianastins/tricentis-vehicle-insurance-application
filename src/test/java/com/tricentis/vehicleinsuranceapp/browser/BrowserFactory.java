package com.tricentis.vehicleinsuranceapp.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class BrowserFactory {

	public WebDriver criarWebDriver() {
		// para rodar htmldriver, adicionar argumento = "htmlunit"
		String webdriver = System.getProperty("browser", "macos chrome");
		switch (webdriver) {
		case "macos firefox":
			return initMacFireforxDriver();
		case "macos chrome":
			return initMacChromeDriver();
		case "macos chromeheadless":
			return initMacChromeDriverHeadless();
		case "windowns firefox":
			return initWindowsFireforxDriver();
		case "windows chrome":
			return initWindowsChromeDriver();
		case "windows chromeheadless":
			return initWindowsChromeDriverHeadless();
		//case "htmlunit
		default:
			return new HtmlUnitDriver(true);
		}
	}

	// os drivers a seguir sao executaveis apenas no sistema operacional mac
	// utilizando GUI
	private WebDriver initMacChromeDriver() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/julianastins/desenvolvimento/eclipse-workspace/vehicleinsuranceapp/drivers/chromedriver");
		return new ChromeDriver();
	}

	// headless browser
	private WebDriver initMacChromeDriverHeadless() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/julianastins/desenvolvimento/eclipse-workspace/sampleapp/drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("window-size=1400,800");
		options.addArguments("headless");
		return new ChromeDriver(options);
	}

	private WebDriver initMacFireforxDriver() {
		System.setProperty("webdriver.gecko.driver",
				"/Users/julianastins/desenvolvimento/eclipse-workspace/sampleapp/drivers/geckodriver");
		return new FirefoxDriver();
	}

	// os drivers a seguir sao executaveis apenas no sistema operacional windows
	// utilizando GUI
	private WebDriver initWindowsChromeDriver() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/julianastins/desenvolvimento/eclipse-workspace/sampleapp/drivers/chromedriverwin32.exe");
		return new ChromeDriver();
	}

	// headless browser
	private WebDriver initWindowsChromeDriverHeadless() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/julianastins/desenvolvimento/eclipse-workspace/sampleapp/drivers/chromedriverwin32.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("window-size=1400,800");
		options.addArguments("headless");
		return new ChromeDriver(options);
	}

	private WebDriver initWindowsFireforxDriver() {
		System.setProperty("webdriver.gecko.driver",
				"/Users/julianastins/desenvolvimento/eclipse-workspace/sampleapp/drivers/geckodriverwin64.exe");
		return new FirefoxDriver();
	}
}
