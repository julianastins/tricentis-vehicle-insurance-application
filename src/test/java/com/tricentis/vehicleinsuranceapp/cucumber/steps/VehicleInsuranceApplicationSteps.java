package com.tricentis.vehicleinsuranceapp.cucumber.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.openqa.selenium.support.PageFactory;

import com.tricentis.vehicleinsuranceapp.browser.Browser;
import com.tricentis.vehicleinsuranceapp.pageobjects.EnterInsurantDataPage;
import com.tricentis.vehicleinsuranceapp.pageobjects.EnterProductDataPage;
import com.tricentis.vehicleinsuranceapp.pageobjects.EnterVehicleDataPage;
import com.tricentis.vehicleinsuranceapp.pageobjects.SelectPriceOptionPage;
import com.tricentis.vehicleinsuranceapp.pageobjects.SendQuotePage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class VehicleInsuranceApplicationSteps {

	Browser acessar;
	EnterVehicleDataPage form1;
	EnterInsurantDataPage form2;
	EnterProductDataPage form3;
	SelectPriceOptionPage form4;
	SendQuotePage form5;
	
	@Given("acesso ao site")
	public void acesso_ao_site() {
		acessar = new Browser();
		acessar.iniciarBrowser("http://sampleapp.tricentis.com/101/app.php");
	}

	@When("prencher formulario da aba Enter Vehicle Data")
	public void prencher_formulario_da_aba_enter_vehicle_data() throws InterruptedException {
		form1 = PageFactory.initElements(Browser.getDriver(), EnterVehicleDataPage.class);
		form1.setSelectMakeOption("Ford");
		form1.selectModelOption("Scooter");
		form1.setCylinderCapacity(1500);
		form1.setEnginePerformance(500);
		form1.setCalendar("07/15/2014");
		form1.selectNumberOfSeatsOption("5");
		form1.selectHightHandDrive("yes");
		form1.selectNumberOfSeatsMotorCycleOption("1");
		form1.selectFuelTypeOption("Diesel");
		form1.setPayLoad(350);
		form1.setTotalWeight(10000);
		form1.setListPrice(5025);
		form1.setLicensePlateNumber("BQ07T1L");
		form1.setAnnualMileage(250);
	}
	
	@When("clicar no botao next_Insurant_Data")
	public void clicar_no_botao_next_insurant_data() {
		form1 = PageFactory.initElements(Browser.getDriver(), EnterVehicleDataPage.class);
		form1.proximoFormulario();    
	}
	@When("prencher formulario da aba Enter Insurant Data")
	public void prencher_formulario_da_aba_enter_insurant_data() {
		form2 = PageFactory.initElements(Browser.getDriver(), EnterInsurantDataPage.class);
		form2.setFirstName("Jhon");
		form2.setLastName("Snow");
		form2.setCalendar("03/15/1980");
		form2.selectGender("male");
		form2.setStreetAdress("18 Sackville Street");
		form2.selectCountry("United Kingdom");
		form2.setZipCode(5436546);
		form2.setCity("London");
		form2.selectOccupation("Public Official");
		form2.selectHobbies(" Skydiving");
		form2.setWebsite("https://www.longislandskydiving.com/");
	}
	@When("clicar no botao next_enter_product_data")
	public void clicar_no_botao_next_enter_product_data() {
		form2 = PageFactory.initElements(Browser.getDriver(), EnterInsurantDataPage.class);
		form2.proximoFormulario();
	}
	@When("prencher formulario da aba Enter Product Data")
	public void prencher_formulario_da_aba_enter_product_data() {
		form3 = PageFactory.initElements(Browser.getDriver(), EnterProductDataPage.class);
		form3.setCalendar("09/28/2021");
		form3.selectInsuranceSum("7.000.000,00");
		form3.selectMeritReting("Bonus 5");
		form3.selectDamageInsurance("Full Coverage");
		form3.selectOptionalProducts("Legal Defense Insurance");
		form3.selectCourtesyCar("Yes");
	}
	@When("clicar no botao next_select_price_option")
	public void clicar_no_botao_next_select_price_option() {
		form3 = PageFactory.initElements(Browser.getDriver(), EnterProductDataPage.class);
		form3.proximoFormulario();
	}
	
	@When("prencher formulario da aba Select Price Option")
	public void prencher_formulario_da_aba_select_price_option() {
		form4 = PageFactory.initElements(Browser.getDriver(), SelectPriceOptionPage.class);
		form4.selectPriceOption("Gold");
	}

	@When("clicar no botao next_send_quote")
	public void clicar_no_botao_next_send_quote() {
		form4 = PageFactory.initElements(Browser.getDriver(), SelectPriceOptionPage.class);
		form4.proximoFormulario();
	}
	
	@When("prencher formulario da aba Send Quote")
	public void prencher_formulario_da_aba_send_quote() {
		form5 = PageFactory.initElements(Browser.getDriver(), SendQuotePage.class);
		form5.setEmail("johnsnow@zipmail.com");
		form5.setPhone("08082734874");
		form5.setUsername("johnny");
		form5.setPassword("Johnny80");
		form5.setConfirmPassword("Johnny80");
		form5.setComments("Desafio Técnico - Teste Selenium Concluido");
	}

	@When("clicar no botao next_send_email")
	public void clicar_no_botao_next_send_email() {
		form5 = PageFactory.initElements(Browser.getDriver(), SendQuotePage.class);
		form5.sendEmail();
	 
	}
	@Then("verificar a mensagem de e-mail enviado com sucesso.")
	public void verificar_a_mensagem_de_e_mail_enviado_com_sucesso() {
		form5 = PageFactory.initElements(Browser.getDriver(), SendQuotePage.class);
		assertEquals("Sending e-mail success!",form5.getMensage());
		form5.clickOkMessage();
		Browser.encerrarBrowser();
	}
}
