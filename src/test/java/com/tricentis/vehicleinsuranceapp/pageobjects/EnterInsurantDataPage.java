package com.tricentis.vehicleinsuranceapp.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.tricentis.vehicleinsuranceapp.util.SeleniumUtil;

public class EnterInsurantDataPage {

	@FindBy(id = "firstname")
	private WebElement txtFirstName;
	@FindBy(id = "lastname")
	private WebElement txtLastName;
	@FindBy(id = "birthdate")
	private WebElement txtBirthDate;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div:nth-child(4) > p > label:nth-child(1) > span")
	private WebElement rbgenderMale;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div:nth-child(4) > p > label:nth-child(2) > span")
	private WebElement rbGenderFemale;
	@FindBy(id = "streetaddress")
	private WebElement txtStreetAddress;
	@FindBy(id = "country")
	private WebElement cbCountry;
	@FindBy(id = "zipcode")
	private WebElement txtZipCode;
	@FindBy(id = "city")
	private WebElement txtCity;
	@FindBy(id = "occupation")
	private WebElement cbOccupation;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(1) > span")
	private WebElement ckHobbiesSpeeding;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(2) > span")
	private WebElement ckHobbiesBungeeJumping;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(3) > span")
	private WebElement ckHobbiesCliffDiving;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(4) > span")
	private WebElement ckHobbiesSkydiving;
	@FindBy(css = "#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(5) > span")
	private WebElement ckHobbiesOther;
	@FindBy(id = "website")
	private WebElement txtWebSite;
	@FindBy(id = "button")
	private WebElement btAnexarImagem;
	@FindBy(id = "nextenterproductdata")
	private WebElement btNextEnterProductData;
	
	
	private String msgExcecao1 = "Informar para este campo apenas letras, e no minimo 2 caracteres";
	private String msgExcecao2 = "Informar apenas numeros ";
	private String msgExcecao3 = "Informar numeros entre 4 e 8 digitos";
	
	public void setFirstName(String name) {
		txtFirstName.clear();
		txtFirstName.sendKeys(name);
	}
	
	public void setLastName(String lastname) {
		txtLastName.clear();
		txtLastName.sendKeys(lastname);
	}
	
	public void setCalendar(String date) {
		txtBirthDate.clear();
		txtBirthDate.sendKeys(date);
		
	}
	
	public void selectGender(String option) {
		if(option.equals("male")) {
			rbgenderMale.click();
		}else if(option.equals("female")) {
			rbGenderFemale.click();
		}
	}
	
	public void setStreetAdress(String adress) {
		txtStreetAddress.clear();
		txtStreetAddress.sendKeys(adress);
	}
	public void selectCountry(String option) {
		cbCountry.click();
		SeleniumUtil.clickElement(cbCountry,option);
	}
	
	public void setZipCode(int value) {
		txtZipCode.clear();
		txtZipCode.sendKeys(Integer.toString(value));
	}
	public void setCity(String city) {
		txtCity.clear();
		txtCity.sendKeys(city);
	}
	
	public void selectOccupation(String option) {
		cbOccupation.click();
		SeleniumUtil.clickElement(cbOccupation,option);
	}
	public void selectHobbies(String option) {
		if(option.equals(" Speeding")) {
			ckHobbiesSpeeding.click();
		}else if(option.equals(" Bungee Jumping")) {
			ckHobbiesBungeeJumping.click();
		}else if(option.equals(" Cliff Diving")) {
			ckHobbiesCliffDiving.click();
		}else if(option.equals(" Skydiving")) {
			ckHobbiesSkydiving.click();
		}else if(option.equals(" Other")) {
			ckHobbiesOther.click();
		}
	}
	
	public void setWebsite(String website) {
		txtWebSite.clear();
		txtWebSite.sendKeys(website);
	}
	
//	public void anexarImagem() {
//		btAnexarImagem.click();
//	}
	
	public void proximoFormulario() {
		btNextEnterProductData.click();
	}
}
