package com.tricentis.vehicleinsuranceapp.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.tricentis.vehicleinsuranceapp.util.SeleniumUtil;

public class EnterProductDataPage {

	@FindBy(id = "startdate")
	private WebElement txtStartDate;
	@FindBy(id = "insurancesum")
	private WebElement cbInsuranceSum;
	@FindBy(id = "meritrating")
	private WebElement cbMeritRating;
	@FindBy(id = "damageinsurance")
	private WebElement cbDamageInsurance;
	@FindBy(css = "#insurance-form > div > section:nth-child(3) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(1) > span")
	private WebElement ckEuroProtection;
	@FindBy(css = "#insurance-form > div > section:nth-child(3) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(2) > span")
	private WebElement ckLegalDefenseInsurance;
	@FindBy(id = "courtesycar")
	private WebElement cbCourtesyCar;
	@FindBy(id = "nextselectpriceoption")
	private WebElement btNextSelectPriceOption;
	
	public void setCalendar(String date) {
		txtStartDate.clear();
		txtStartDate.sendKeys(date);
		
	}
	public void selectInsuranceSum(String value) {
		cbInsuranceSum.click();
		SeleniumUtil.clickElement(cbInsuranceSum,value);
	}
	
	public void selectMeritReting(String option) {
		cbMeritRating.click();
		SeleniumUtil.clickElement(cbMeritRating,option);
	}
	public void selectDamageInsurance(String option) {
		cbDamageInsurance.click();
		SeleniumUtil.clickElement(cbDamageInsurance,option);
	}
	public void selectOptionalProducts(String option) {
		if(option.equals("Euro Protection")) {
			ckEuroProtection.click();
		}else if(option.equals("Legal Defense Insurance")) {
			ckLegalDefenseInsurance.click();
		}
	}
	public void selectCourtesyCar(String option) {
		cbCourtesyCar.click();
		SeleniumUtil.clickElement(cbCourtesyCar,option);
	}
	public void proximoFormulario() {
		btNextSelectPriceOption.click();
	}
}
