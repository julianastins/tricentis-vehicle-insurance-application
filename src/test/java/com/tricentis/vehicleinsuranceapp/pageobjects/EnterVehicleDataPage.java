package com.tricentis.vehicleinsuranceapp.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.tricentis.vehicleinsuranceapp.util.SeleniumUtil;

public class EnterVehicleDataPage {

	@FindBy(css = "#make")
	private WebElement cbMake;
	@FindBy(id = "model")
	private WebElement cbModel;
	@FindBy(id = "cylindercapacity")
	private WebElement txtCilinderCapacity;
	@FindBy(id = "engineperformance")
	private WebElement txtEnginePerformance;
	@FindBy(id = "dateofmanufacture")
	private WebElement dateManufacture;
	@FindBy(xpath = "//*[@id=\"numberofseats\"]")
	private WebElement cbNumberOfSeats;
	@FindBy(css = "#insurance-form > div > section:nth-child(1) > div:nth-child(7) > p > label:nth-child(1) > span")
	private WebElement rbYes;
	@FindBy(css = "#insurance-form > div > section:nth-child(1) > div:nth-child(7) > p > label:nth-child(2) > span")
	private WebElement rbNo;
	@FindBy(css = "#numberofseatsmotorcycle")
	private WebElement cbSeatsMotorCycle;
	@FindBy(id = "payload")
	private WebElement txtPayload;
	@FindBy(id = "totalweight")
	private WebElement txtTotalWeight;
	@FindBy(id = "fuel")
	private WebElement cbFuelType;
	@FindBy(id = "listprice")
	private WebElement txtListPrice;
	@FindBy(id = "licenseplatenumber")
	private WebElement txtLicensePlateNumber;
	@FindBy(id = "annualmileage")
	private WebElement txtAnnualMileage;
	@FindBy(id = "nextenterinsurantdata")
	private WebElement btNext;

	private String msgExcecao2 = "Informar numero entre 1 e 2000";
	private String msgExcecao3 = "Informar numero entre 500 e 100000";
	private String msgExcecao4 = "Informar numero entre 100 e 100000";
	private String msgExcecao5 = "Informar numero entre 1 e 1000";
	private String msgExcecao6 = "Informar numero entre 100 e 50000";
	private String msgExcecao7 = "Escolha Sim ou Nao";
	
	public void setSelectMakeOption(String option) {
		cbMake.click();
		SeleniumUtil.clickElement(cbMake, option);
	}
	
	public void selectModelOption(String option) {
		cbModel.click();
		SeleniumUtil.clickElement(cbModel, option);
	}

	public void setCylinderCapacity(int value) {
		txtCilinderCapacity.clear();
		if (value > 1 && value < 2000) {
			txtCilinderCapacity.sendKeys(Integer.toString(value));
		} else {
			throw new RuntimeException(msgExcecao2);
		}
	}
	
	public void setEnginePerformance(int value) {
		txtEnginePerformance.clear();
		if (value > 1 && value < 2000) {
			txtEnginePerformance.sendKeys(Integer.toString(value));
		} else {
			throw new RuntimeException(msgExcecao2);
		}
	}

	public void setCalendar(String date) {
		dateManufacture.clear();
		dateManufacture.sendKeys(date);
		
	}

	public void selectNumberOfSeatsOption(String option) {
		cbNumberOfSeats.click();
		SeleniumUtil.clickElement(cbNumberOfSeats, option);
	}
	
	public void selectHightHandDrive(String option){
		if(option.equals("yes")) {
			rbYes.click();
		}else if(option.equals("no")) {
			rbNo.click();
		}else {
			throw new RuntimeException(msgExcecao7);
		}
	}
	
	public void selectNumberOfSeatsMotorCycleOption(String option){

		cbSeatsMotorCycle.click();
		SeleniumUtil.clickElement(cbSeatsMotorCycle,option);
}

	public void selectFuelTypeOption(String option) {
		cbFuelType.click();
		SeleniumUtil.clickElement(cbFuelType, option);
	}
	
	public void setPayLoad(int value) {
		txtPayload.clear();
		if (value > 1 && value < 1000) {
			txtPayload.sendKeys(Integer.toString(value));
		} else {
			throw new RuntimeException(msgExcecao5);
		}
	}
	
	public void setTotalWeight(int value) {
		txtTotalWeight.clear();
		if (value > 100 && value < 50000) {
			txtTotalWeight.sendKeys(Integer.toString(value));
		} else {
			throw new RuntimeException(msgExcecao6);
		}
	}
	public void setListPrice(int value) {
		txtListPrice.clear();
		if (value > 500 && value < 100000) {
			txtListPrice.sendKeys(Integer.toString(value));
		} else {
			throw new RuntimeException(msgExcecao3);
		}
	}

	public void setLicensePlateNumber(String value) {
		txtLicensePlateNumber.clear();
		txtLicensePlateNumber.sendKeys(value);
	}

	public void setAnnualMileage(int value) {
		txtAnnualMileage.clear();
		if (value > 100 && value < 100000) {
			txtAnnualMileage.sendKeys(Integer.toString(value));
		} else {
			throw new RuntimeException(msgExcecao4);
		}
	}
	public void proximoFormulario() {
		btNext.click();
	}
}
