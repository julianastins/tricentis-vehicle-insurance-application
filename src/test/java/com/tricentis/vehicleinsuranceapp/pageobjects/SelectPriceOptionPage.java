package com.tricentis.vehicleinsuranceapp.pageobjects;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SelectPriceOptionPage {

	@FindBy(css = "#priceTable > tfoot > tr > th.group > label:nth-child(1) > span")
	private WebElement rbOptionSilver;
	@FindBy(css = "#priceTable > tfoot > tr > th.group > label:nth-child(2) > span")
	private WebElement rbOptionGold;
	@FindBy(css = "#priceTable > tfoot > tr > th.group > label:nth-child(3) > span")
	private WebElement rbOptionPlatinum;
	@FindBy(css = "#priceTable > tfoot > tr > th.group > label:nth-child(4) > span")
	private WebElement rbOptionUltimate;
	@FindBy(id = "nextsendquote")
	private WebElement btNextSendQuote;
	
	public void selectPriceOption(String option) {
		if(option.equals("Silver")) {
			rbOptionSilver.click();
		}else if(option.equals("Gold")) {
			rbOptionGold.click();
		}else if(option.equals("Platinum")) {
			rbOptionPlatinum.click();
		}else if(option.equals("Ultimate")) {
			rbOptionUltimate.click();
		}
	}
		
	public void proximoFormulario() {
		btNextSendQuote.click();
	}
}
