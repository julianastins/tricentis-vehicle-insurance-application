package com.tricentis.vehicleinsuranceapp.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SendQuotePage {


	@FindBy(id = "email")
	private WebElement txtEmail;
	@FindBy(id = "phone")
	private WebElement txtPhone;
	@FindBy(id = "username")
	private WebElement txtUsername;
	@FindBy(id = "password")
	private WebElement txtPassword;
	@FindBy(id = "confirmpassword")
	private WebElement txtConfirmPassword;
	@FindBy(id = "Comments")
	private WebElement txtComments;
	@FindBy(id = "sendemail")
	private WebElement btSendemail;
	@FindBy(css = "body > div.sweet-alert.showSweetAlert.visible > h2")
	private WebElement msgSuccess;
	@FindBy(css = "body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button")
	private WebElement btOkmsg;

	public void setEmail(String email) {
		txtEmail.clear();
		txtEmail.sendKeys(email);
	}

	public void setPhone(String phone) {
		txtPhone.clear();
		txtPhone.sendKeys(phone);
	}

	public void setUsername(String username) {
		txtUsername.clear();
		txtUsername.sendKeys(username);
	}

	public void setPassword(String password) {
		txtPassword.clear();
		txtPassword.sendKeys(password);
	}

	public void setConfirmPassword(String confirmpassword) {
		txtConfirmPassword.clear();
		txtConfirmPassword.sendKeys(confirmpassword);
	}

	public void setComments(String comments) {
		txtComments.clear();
		txtComments.sendKeys(comments);
	}

	public void sendEmail() {
		btSendemail.click();
	}

	public String getMensage() {
		String mensage = msgSuccess.getText();
		return mensage;
	}
	
	public void clickOkMessage() {
		btOkmsg.click();
	}
}
