package com.tricentis.vehicleinsuranceapp.util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SeleniumUtil {
	
	public static void clickElement(WebElement element, String option) {
		List<WebElement> list = element.findElements(By.tagName("option"));
		for (WebElement item : list) {
			if (item.getText().equals(option)) {
				item.click();
				break;
			} 		
		}
	}
}
