Feature: Preencher os formularios da pagina Vehicle Insurance Application
 Desejo acessar o site da trecentis, preencher os formulario existentes em cada aba, clicar em next e ao final enviar um e-mail

  Scenario: Vehicle Insurance Application
    Given acesso ao site
    When prencher formulario da aba Enter Vehicle Data
    When  clicar no botao next_Insurant_Data
    When prencher formulario da aba Enter Insurant Data
    When clicar no botao next_enter_product_data
    When prencher formulario da aba Enter Product Data
    When clicar no botao next_select_price_option
    When prencher formulario da aba Select Price Option
    When clicar no botao next_send_quote
    When prencher formulario da aba Send Quote
    When clicar no botao next_send_email
    Then verificar a mensagem de e-mail enviado com sucesso.